<?php
/**
 * Created by PhpStorm.
 * Author: baihu
 * Date: 2019/12/16
 * Time: 11:37
 */

namespace app\admin\controller;

use Logic\KefuLogic;
use Logic\Visitor;
use Logic\VisitorService;
class Index extends Base
{
    public  function  index(){
        $this->assign([
            'name'=>session('admin_name'),
        ]);
        return $this->fetch();

    }

    public  function  home(){
        //获取所有客服数量
          $kefu_num=KefuLogic::kefuCount();
          //获取在线客服数量
        $online_kefu_num=KefuLogic::kefuCount(['online_status'=>1]);
        //获取当前在线游客数量
        $online_visitor_num=Visitor::visitorCount(['online_status'=>1]);
        //获取当天的会话次数
        $service_num=VisitorService::getServiceNum();
        $days=  date('t');
        $service_num_list=[];
        for($i=0;$i<$days;$i++){
            $start_time = strtotime(date('Y-m-01'));  //获取本月第一天时间戳
            $date=date('Y-m-d', $start_time+$i*86400);
            $endDate=date('Y-m-d 23:59:59',strtotime($date));
            $where1[0]=['create_time','between time',[$date, $endDate]];

            $service_num_list[]=[
                'date'=>$date,
                'new_kefu_num'=>KefuLogic::kefuCount($where1),
                'new_visitor_num'=>Visitor::visitorCount($where1),
                'add_service_num'=>VisitorService::getServiceNum($date),
            ];
        }
          $this->assign([
              'kefu_num'=>$kefu_num,
              'online_kefu_num'=>$online_kefu_num,
              'online_visitor_num'=>$online_visitor_num,
              'service_num'=>$service_num,
              'date_list'=>json_encode(array_column($service_num_list,'date')),
              'new_kefu_num'=>json_encode(array_column($service_num_list,'new_kefu_num')),
              'new_visitor_num'=>json_encode(array_column($service_num_list,'new_visitor_num')),
              'add_service_num'=>json_encode(array_column($service_num_list,'add_service_num')),
          ]);
         return $this->fetch();
    }

}
